from setuptools import setup

setup(
    name='PyPow',
    packages=['PyPow'],
    include_package_data=True,
    install_requires=[
        'flask',
        'zeep',
    ],
)
