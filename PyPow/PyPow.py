from flask import Flask, g, request, render_template
from PyPow.dbproxy import StationDB
from PyPow.snotel import Snotel
import os

app = Flask(__name__)
app.config.from_object(__name__) #load config from this file

#Load default config and override config from an environemtn variable
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'snotel.db'),
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))
app.config.from_envvar('PYPOW_SETTINGS', silent=True)

@app.cli.command('initdb')
def initdb_command():
    """Initializes the database."""
    StationDB(app.config['DATABASE']).init_db()
    print('Initialized the database.')

def getDBProxy(databaseFile):
    if not hasattr(g, 'db_proxy'):
        g.db_proxy=StationDB(databaseFile)
    return g.db_proxy

@app.route('/pypow')
def pypow():
    sdb = StationDB()
    stations = sdb.get_stations()
    return render_template('pypow.html', stations = stations)

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/details/<stationTriplet>')
def getDetails(stationTriplet):
    webservice = Snotel()
    stationData = webservice.getStationMetadata(stationTriplet)
    snowDepth = webservice.getSnowDepth(stationTriplet)
    if not snowDepth:
        snowDepth = "No Depth Data"
    return render_template('details.html', stationData = stationData, snowDepth = snowDepth)

if __name__ == '__main__':
    app.run(debug=True)
