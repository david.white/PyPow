from zeep import Client
import datetime

class Snotel(object):
    ''' Zeep based wrapper for the AWDB service for snotel station data '''

    def __init__(self, wsdl='snotel.wsdl'):
        self.client = Client(wsdl=wsdl)

    def getStations(self, stationIds=None, stateCds=None, networkCds=None, hucs=None,
                    countyNames=None, minLatitude=None, maxLatitude=None, minLongitude=None, maxLongitude=None,
                    minElevation=None, maxElevation=None, elementCds=None, ordinals=None, heightDepths=None, logicalAnd=True):
        return self.client.service.getStations(stationIds=stationIds, 
                                                stateCds=stateCds,
                                                networkCds=networkCds,
                                                hucs=hucs,
                                                countyNames=countyNames,
                                                minLatitude=minLatitude,
                                                maxLatitude=maxLatitude,
                                                minLongitude=minLongitude,
                                                maxLongitude=maxLongitude,
                                                minElevation=minElevation,
                                                maxElevation=maxElevation,
                                                elementCds=elementCds,
                                                ordinals=ordinals,
                                                heightDepths=heightDepths,
                                                logicalAnd=logicalAnd)

    def getStationMetadata(self, stationTriplet):
        return self.client.service.getStationMetadata(stationTriplet=stationTriplet)

    def getStationMetadataMultiple(self, stationTriplets):
        return self.client.service.getStationMetadataMultiple(stationTriplets)

    def getData(self, stationTriplets, elementCd, ordinal, heightDepth, duration, getFlags, beginDate, endDate, alwaysReturnDailyFeb29=False):
        return self.client.service.getData(stationTriplets=stationTriplets, 
                                            elementCd=elementCd, 
                                            ordinal=ordinal, 
                                            heightDepth=heightDepth,
                                            duration=duration, 
                                            getFlags=getFlags, 
                                            beginDate=beginDate, 
                                            endDate=endDate,
                                            alwaysReturnDailyFeb29=alwaysReturnDailyFeb29)

    # Element Codes: https://www.wcc.nrcs.usda.gov/web_service/AWDB_Web_Service_Reference.htm#commonlyUsedElementCodes
    def getInstantaneousData(self, stationTriplets, elementCd, ordinal, heightDepth, beginDate, endDate, snowfilter, unitSystem):
        return self.client.service.getInstantaneousData(stationTriplets=stationTriplets, 
                                                        elementCd=elementCd, 
                                                        ordinal=ordinal, 
                                                        heightDepth=heightDepth,
                                                        beginDate=beginDate, 
                                                        endDate=endDate, 
                                                        filter=snowfilter,
                                                        unitSystem=unitSystem)

    def getSnowDepth(self, stationTriplet):
        yesterday = (datetime.date.today() - datetime.timedelta(1)).isoformat()
        today = datetime.date.today().isoformat()
        webResponse = self.client.service.getData(stationTriplet, 'SNWD', 
                                            ordinal=1, 
                                            heightDepth=None, 
                                            duration='DAILY', 
                                            getFlags=False, 
                                            beginDate=yesterday, 
                                            endDate=today)
        try:
            depth = webResponse[0].values[1]       
        except IndexError:
            depth = None                                     
        return depth

    def getAccumulatedPrecip(self, stationTriplet):
        today = datetime.date.today().isoformat()
        return self.client.service.getData(stationTriplet, 'PREC', 
                                            ordinal=1, 
                                            heightDepth=None, 
                                            duration='DAILY', 
                                            getFlags=False, 
                                            beginDate="2017-10-01", 
                                            endDate=today)

# def main():    
#     stations=snotel.getStations(stateCds=['CO'])
#     stationList = []
#     metadata = snotel.getStationMetadataMultiple(stations)
#     for station in metadata:
#         print(station.stationTriplet, station.latitude, station.longitude,
#                 station.elevation, 'CO', station.name, station.beginDate, station.endDate)
# #   print(stations)

if __name__ == '__main__':
    snotel=Snotel()
    print(snotel.getStationMetadata("06016300:CO:BOR"))
    print(snotel.getSnowDepth("1186:CO:SNTL"))
    print(snotel.getSnowDepth("06016300:CO:BOR")) #No data 
    #print(snotel.getAccumulatedPrecip("1186:CO:SNTL"))
