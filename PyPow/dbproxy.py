import sqlite3
from flask import Flask, g
from PyPow.snotel import Snotel

app = Flask(__name__)

class StationDB(object):
    def __init__(self, db='snotel.db',):
        self.dbfile=db
        self.db = self.get_db()

    def init_db(self):
        with app.open_resource('schema.sql', mode='r') as f:
            self.db.cursor().executescript(f.read())
        self.db.commit()
        self.loadDB()

    def get_db(self):
        if not hasattr(g, 'sqlite_db'):
            g.sqlite_db = self.connect_db()
        return g.sqlite_db

    def connect_db(self):
        rv = sqlite3.connect(self.dbfile)
        rv.row_factory = sqlite3.Row
        return rv

    def get_stations(self):
        cursor = self.db.execute('select name, longitude, latitude, elevation, triplet from snotel')
        return cursor.fetchall()

    def loadDB(self):
        snotelProxy = Snotel()
        #stationList = snotelProxy.getStations(stateCds=['CO'])
        stationList= snotelProxy.getStations()
        allMetadata = snotelProxy.getStationMetadataMultiple(stationList)
        # { #EXAMPLE METADATA ENTRY
        # 'actonId': '06J19S',
        # 'beginDate': '2002-08-14 06:00:00',
        # 'countyName': 'Jackson',
        # 'elevation': Decimal('9340.00'),
        # 'endDate': '2100-01-01 00:00:00',
        # 'fipsCountryCd': 'US',
        # 'fipsCountyCd': '057',
        # 'fipsStateNumber': '08',
        # 'huc': '101800010202',
        # 'hud': '10180001',
        # 'latitude': Decimal('40.79488'),
        # 'longitude': Decimal('-106.59535'),
        # 'name': 'Zirkel',
        # 'shefId': 'ZIRC2',
        # 'stationDataTimeZone': Decimal('-8.0'),
        # 'stationTimeZone': None,
        # 'stationTriplet': '1033:CO:SNTL'
        # }
        skipped = 0
        inserted = 0            
        for station in allMetadata:
            try:
                self.db.execute("insert into snotel (triplet, latitude, longitude,\
                                elevation, stateCode, name, beginDate, endDate) values (?,?,?,?,?,?,?,?)",
                                [station.stationTriplet, str(station.latitude), str(station.longitude),
                                str(station.elevation), 'CO', station.name, station.beginDate, station.endDate])
            except Exception as e:
                print(e, skipped, "skipped")
                skipped += 1
            else:
                print("Inserted", station.name)
                inserted += 1
            print(inserted, "Records inserted")
            self.db.commit()

            

