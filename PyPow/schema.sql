drop table if exists snotel;
CREATE TABLE IF NOT EXISTS snotel (
    triplet text primary key,
    latitude real,
    longitude real,
    elevation integer,
    stateCode text,
    name text,
    beginDate text,
    endDate, text   
);
